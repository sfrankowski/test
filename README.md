Installation:

```bash
npm install
```

and 

```
bower install
```

If you don't have http-server, just install it


```bash
npm install -g http-server
```

Then run web sockets server:

```bash
cd server
node server.js
```

and 

```bash
grunt serve
```
To build application, just 

```bash
grunt build
```

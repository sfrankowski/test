var socket;

var common = {
    width: 490,
    chartArea: {
        top: 30,
        left: 44,
        width: 499,
        height: 108
    }
};
var data0;
google.load("visualization", "1", {
    packages: ['corechart', 'bar']
});

function drawChart(dt) {

    var options = {
        //  title: none,'Reneuve per week',
        //axisTitlesPosition: 'in',
        chartArea: common.chartArea,
        width: 499,
        left: 44,
        //   top: 21,
        //            titleTextStyle: {
        //                color: '#757575',
        //                fontName: 'Roboto2',
        //                bold: false,
        //                fontSize: 10
        //            },
        animation: {
            startup: true,
            duration: 1000,
            easing: 'out'
        },



        //            legend: {
        //                position: 'bottom'
        //            },

        legend: null,

        vAxis: {
            minValue: 0,
            maxValue: 1000,
            ticks: [{
                v: 0,
                f: '$'
            }, 500, 1000]
        },


        curveType: 'line',
        pointSize: 7,
        dataOpacity: 0.3,
    };


    var options2 = {
        title: 'Installations per week',
        axisTitlesPosition: 'in',
        legend: {
            position: 'bottom'
        },

        titleTextStyle: {
            color: '#757575',
            fontName: 'Arial',
            bold: false,
            fontSize: 10
        },
        chartArea: common.chartArea,
        width: 499,

        hAxis: {
            textStyle: {
                color: 'black',
                fontName: 'Arial',
                fontSize: 10
            }

            //                ,
            //                textPosition: 'in'
        }
    };






    var chart0 = new google.visualization.LineChart(document.getElementById('chart0'));
    var chart1 = new google.visualization.ColumnChart(document.getElementById('chart1'));



    google.visualization.events.addListener(chart0, 'ready', function () {

        document.querySelector('div.panel-title').classList.remove('invisible');
        document.querySelector('div.panel-legend-wrapper').classList.remove('invisible');




    });

    google.visualization.events.addListener(chart0, 'error', function (d, e) {
        console.log('[[[[[[[[[[[[');
        console.log(d);
        console.log(e);
        console.log(']]]]]]]]]]]]');

    });



    google.visualization.events.addListener(chart1, 'ready', function () {
        //fix position of the title

        document.querySelector('#chart1 svg g text').setAttribute('x', '392');
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(6) > text ').textContent = '';
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(7) > text ').textContent = '';
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(8) > text ').textContent = '5';
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(8) > text ').setAttribute('x', '38');
        var y8 = document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(8) > text ').getAttribute('y');
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(8) > text ').setAttribute('y', parseInt(y8) + 11);

        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(9) > text ').textContent = '';
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(10) > text ').textContent = '10';
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(10) > text ').setAttribute('x', '38');

        var y10 = document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(10) > text ').getAttribute('y');
        document.querySelector('#chart1 svg > g:nth-child(5) > g:nth-child(4) > g:nth-child(10) > text ').setAttribute('y', parseInt(y10) + 11);




    });

    var __aj2 = new __ajax('data/data1.json');


    __aj2.get().then(function (dt) {

        var dataArr = google.visualization.arrayToDataTable(JSON.parse(dt));
        chart1.draw(dataArr, options2);
    }, function () {});



    socket = io.connect('http://localhost:3000');

    socket.on('connect', function () {

    });

    socket.on('initData', function (data) {

        data0 = google.visualization.arrayToDataTable(data);
        chart0.draw(data0, options);


    });
    socket.on('data', function (data, err) {

        data0.removeRow(0);
        data0.addRow(data);
        console.log(data0.toString());
        chart0.draw(data0, options);
    });

}





var run = function run() {


    google.setOnLoadCallback(drawChart);
    //debugger;



};

run();